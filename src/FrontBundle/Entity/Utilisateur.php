<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;


/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\UtilisateurRepository")
 */
class Utilisateur extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=20, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=20, nullable=true)
     */
    private $prenom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateNaissance", type="date", length=20, nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\OneToMany(targetEntity="FrontBundle\Entity\FicheFrais",mappedBy="utilisateur", cascade={"persist", "remove"});
     */
    private $fichesFrais;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->fichesFrais = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Utilisateur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Utilisateur
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set dateNaissance
     *
     * @param \DateTime $dateNaissance
     *
     * @return Utilisateur
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return \DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Add fichesFrai
     *
     * @param \FrontBundle\Entity\FicheFrais $fichesFrai
     *
     * @return Utilisateur
     */
    public function addFicheFrais(\FrontBundle\Entity\FicheFrais $ficheFrais)
    {
        $this->fichesFrais[] = $ficheFrais;

        return $this;
    }

    /**
     * Remove fichesFrai
     *
     * @param \FrontBundle\Entity\FicheFrais $ficheFrais
     */
    public function removeFicheFrais(\FrontBundle\Entity\FicheFrais $ficheFrais)
    {
        $this->fichesFrais->removeElement($ficheFrais);
    }

    /**
     * Get fichesFrais
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFichesFrais()
    {
        return $this->fichesFrais;
    }

    /**
     * Add fichesFrai
     *
     * @param \FrontBundle\Entity\FicheFrais $fichesFrai
     *
     * @return Utilisateur
     */
    public function addFichesFrai(\FrontBundle\Entity\FicheFrais $fichesFrai)
    {
        $this->fichesFrais[] = $fichesFrai;

        return $this;
    }

    /**
     * Remove fichesFrai
     *
     * @param \FrontBundle\Entity\FicheFrais $fichesFrai
     */
    public function removeFichesFrai(\FrontBundle\Entity\FicheFrais $fichesFrai)
    {
        $this->fichesFrais->removeElement($fichesFrai);
    }
}

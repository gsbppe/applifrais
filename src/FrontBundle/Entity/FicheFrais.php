<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FicheFrais
 *
 * @ORM\Table(name="fiche_frais")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\FicheFraisRepository")
 */
class FicheFrais
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="mois", type="integer")
     */
    private $mois;

    /**
     * @var int
     *
     * @ORM\Column(name="annee", type="integer")
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="FrontBundle\Entity\Etat", inversedBy="fichesFrais");
     *
     *
     */
    private $etat;

    /**
     * @ORM\ManyToOne(targetEntity="FrontBundle\Entity\Utilisateur", inversedBy="fichesFrais");
     *
     *
     */
    private $utilisateur;

    /**
     * @ORM\OneToMany(targetEntity="FrontBundle\Entity\FraisForfait", mappedBy="ficheFrais", cascade={"persist", "remove"});
     *
     *
     */
    private $fraisForfaits;
    /**
     * @ORM\OneToMany(targetEntity="FrontBundle\Entity\FraisHorsForfait", mappedBy="ficheFrais", cascade={"persist", "remove"});
     *
     *
     */
    private $fraisHorsForfaits;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fraisForfaits = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fraisHorsForfaits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mois
     *
     * @param integer $mois
     *
     * @return FicheFrais
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return integer
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set annee
     *
     * @param integer $annee
     *
     * @return FicheFrais
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return integer
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set etat
     *
     * @param string $etat
     *
     * @return FicheFrais
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set utilisateur
     *
     * @param \FrontBundle\Entity\Utilisateur $utilisateur
     *
     * @return FicheFrais
     */
    public function setUtilisateur(\FrontBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \FrontBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Add fraisForfait
     *
     * @param \FrontBundle\Entity\FraisForfait $fraisForfait
     *
     * @return FicheFrais
     */
    public function addFraisForfait(\FrontBundle\Entity\FraisForfait $fraisForfait)
    {
        $this->fraisForfaits[] = $fraisForfait;
        $fraisForfait->setFicheFrais($this);

        return $this;
    }

    /**
     * Remove fraisForfait
     *
     * @param \FrontBundle\Entity\FraisForfait $fraisForfait
     */
    public function removeFraisForfait(\FrontBundle\Entity\FraisForfait $fraisForfait)
    {
        $this->fraisForfaits->removeElement($fraisForfait);
    }

    /**
     * Get fraisForfaits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFraisForfaits()
    {
        return $this->fraisForfaits;
    }

    /**
     * Add fraisHorsForfait
     *
     * @param \FrontBundle\Entity\FraisHorsForfait $fraisHorsForfait
     *
     * @return FicheFrais
     */
    public function addFraisHorsForfait(\FrontBundle\Entity\FraisHorsForfait $fraisHorsForfait)
    {
        $this->fraisHorsForfaits[] = $fraisHorsForfait;
        $fraisHorsForfait->setFicheFrais($this);

        return $this;
    }

    /**
     * Remove fraisHorsForfait
     *
     * @param \FrontBundle\Entity\FraisHorsForfait $fraisHorsForfait
     */
    public function removeFraisHorsForfait(\FrontBundle\Entity\FraisHorsForfait $fraisHorsForfait)
    {
        $this->fraisHorsForfaits->removeElement($fraisHorsForfait);
    }

    /**
     * Get fraisHorsForfaits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFraisHorsForfaits()
    {
        return $this->fraisHorsForfaits;
    }
}

<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Etat
 *
 * @ORM\Table(name="etat")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\EtatRepository")
 */
class Etat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=50)
     */
    private $libelle;

    /**
     * @var int
     *
     * @ORM\Column(name="ordre", type="integer")
     */
    private $ordre;

    /**
     * @ORM\OneToMany(targetEntity="FrontBundle\Entity\FraisForfait", mappedBy="etat");
     *
     *
     */

    private $fraisForfaits;

    /**
     * @ORM\OneToMany(targetEntity="FrontBundle\Entity\FraisHorsForfait", mappedBy="etat");
     *
     *
     */
    private $fraisHorsForfaits;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fraisForfaits = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fraisHorsForfaits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Etat
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     *
     * @return Etat
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre
     *
     * @return integer
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Add fraisForfait
     *
     * @param \FrontBundle\Entity\FraisForfait $fraisForfait
     *
     * @return Etat
     */
    public function addFraisForfait(\FrontBundle\Entity\FraisForfait $fraisForfait)
    {
        $this->fraisForfaits[] = $fraisForfait;

        return $this;
    }

    /**
     * Remove fraisForfait
     *
     * @param \FrontBundle\Entity\FraisForfait $fraisForfait
     */
    public function removeFraisForfait(\FrontBundle\Entity\FraisForfait $fraisForfait)
    {
        $this->fraisForfaits->removeElement($fraisForfait);
    }

    /**
     * Get fraisForfaits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFraisForfaits()
    {
        return $this->fraisForfaits;
    }

    /**
     * Add fraisHorsForfait
     *
     * @param \FrontBundle\Entity\FraisHorsForfait $fraisHorsForfait
     *
     * @return Etat
     */
    public function addFraisHorsForfait(\FrontBundle\Entity\FraisHorsForfait $fraisHorsForfait)
    {
        $this->fraisHorsForfaits[] = $fraisHorsForfait;

        return $this;
    }

    /**
     * Remove fraisHorsForfait
     *
     * @param \FrontBundle\Entity\FraisHorsForfait $fraisHorsForfait
     */
    public function removeFraisHorsForfait(\FrontBundle\Entity\FraisHorsForfait $fraisHorsForfait)
    {
        $this->fraisHorsForfaits->removeElement($fraisHorsForfait);
    }

    /**
     * Get fraisHorsForfaits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFraisHorsForfaits()
    {
        return $this->fraisHorsForfaits;
    }
}

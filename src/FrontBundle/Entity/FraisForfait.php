<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FraisForfait
 *
 * @ORM\Table(name="frais_forfait")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\FraisForfaitRepository")
 */
class FraisForfait
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_frais", type="date")
     */
    private $dateFrais;
    /**
     * @ORM\ManyToOne(targetEntity="FrontBundle\Entity\FicheFrais", inversedBy="fraisForfaits");
     *
     *
     */
    private $ficheFrais;

    /**
     * @ORM\ManyToOne(targetEntity="FrontBundle\Entity\FraisForfaitType", inversedBy="fraisForfaits");
     *
     *
     */
    private $fraisForfaitType;

    /**
     * @ORM\ManyToOne(targetEntity="FrontBundle\Entity\Etat", inversedBy="fraisForfaits");
     *
     *
     */
    private $etat;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return FraisForfait
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return FraisForfait
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set ficheFrais
     *
     * @param \FrontBundle\Entity\FicheFrais $ficheFrais
     *
     * @return FraisForfait
     */
    public function setFicheFrais(\FrontBundle\Entity\FicheFrais $ficheFrais = null)
    {
        $this->ficheFrais = $ficheFrais;

        return $this;
    }

    /**
     * Get ficheFrais
     *
     * @return \FrontBundle\Entity\FicheFrais
     */
    public function getFicheFrais()
    {
        return $this->ficheFrais;
    }

    /**
     * Set fraisForfaitType
     *
     * @param \FrontBundle\Entity\FraisForfaitType $fraisForfaitType
     *
     * @return FraisForfait
     */
    public function setFraisForfaitType(\FrontBundle\Entity\FraisForfaitType $fraisForfaitType = null)
    {
        $this->fraisForfaitType = $fraisForfaitType;

        return $this;
    }

    /**
     * Get fraisForfaitType
     *
     * @return \FrontBundle\Entity\FraisForfaitType
     */
    public function getFraisForfaitType()
    {
        return $this->fraisForfaitType;
    }

    /**
     * Set dateFrais
     *
     * @param \DateTime $dateFrais
     *
     * @return FraisForfait
     */
    public function setDateFrais($dateFrais)
    {
        $this->dateFrais = $dateFrais;

        return $this;
    }

    /**
     * Get dateFrais
     *
     * @return \DateTime
     */
    public function getDateFrais()
    {
        return $this->dateFrais;
    }

    /**
     * Set etat
     *
     * @param \FrontBundle\Entity\Etat $etat
     *
     * @return FraisForfait
     */
    public function setEtat(\FrontBundle\Entity\Etat $etat = null)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return \FrontBundle\Entity\Etat
     */
    public function getEtat()
    {
        return $this->etat;
    }
}

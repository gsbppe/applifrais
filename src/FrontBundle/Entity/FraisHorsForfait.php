<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FraisHorsForfait
 *
 * @ORM\Table(name="frais_hors_forfait")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\FraisHorsForfaitRepository")
 */
class FraisHorsForfait
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=50, nullable=true)
     */
    private $libelle;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_frais", type="date")
     */
    private $dateFrais;

    /**
     * @ORM\ManyToOne(targetEntity="FrontBundle\Entity\FicheFrais", inversedBy="fraisForfaits");
     *
     *
     */
    private $ficheFrais;

    /**
     * @ORM\ManyToOne(targetEntity="FrontBundle\Entity\Etat", inversedBy="fraisHorsForfaits");
     *
     *
     */
    private $etat;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return FraisHorsForfait
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return FraisHorsForfait
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set dateFrais
     *
     * @param \DateTime $dateFrais
     *
     * @return FraisHorsForfait
     */
    public function setDateFrais($dateFrais)
    {
        $this->dateFrais = $dateFrais;

        return $this;
    }

    /**
     * Get dateFrais
     *
     * @return \DateTime
     */
    public function getDateFrais()
    {
        return $this->dateFrais;
    }

    /**
     * Set ficheFrais
     *
     * @param \FrontBundle\Entity\FicheFrais $ficheFrais
     *
     * @return FraisHorsForfait
     */
    public function setFicheFrais(\FrontBundle\Entity\FicheFrais $ficheFrais = null)
    {
        $this->ficheFrais = $ficheFrais;

        return $this;
    }

    /**
     * Get ficheFrais
     *
     * @return \FrontBundle\Entity\FicheFrais
     */
    public function getFicheFrais()
    {
        return $this->ficheFrais;
    }

    /**
     * Set etat
     *
     * @param \FrontBundle\Entity\Etat $etat
     *
     * @return FraisHorsForfait
     */
    public function setEtat(\FrontBundle\Entity\Etat $etat = null)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return \FrontBundle\Entity\Etat
     */
    public function getEtat()
    {
        return $this->etat;
    }
}

<?php

namespace FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UtilisateurType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('dateNaissance')
            /*->add('civilite',ChoiceType::class, array(
                    'choices'  => array(
                        'M' => 'm',
                        'F' => 'f',
                    ),
                    'expanded' => true, //affichage liste déroulante ou pas
                    'multiple' => true,
                )
            )*/
            ->add('email')
            ->add('username')
            ->add('enabled')
            ->add('plainPassword',  RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe doivent être identique.',
                // 'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Répéter Mot de passe'),
            ))
            ->add('roles', ChoiceType::class, array(
                    'choices'  => array(
                        'ADMIN' => 'ROLE_ADMIN',
                        'COMPTABLE' => 'ROLE_COMPTABLE',
                        'VISITEUR' => 'ROLE_VISITEUR'
                    ),
                    'expanded' => true, //affichage liste déroulante ou pas
                    'multiple' => true,
                )
            )

        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FrontBundle\Entity\Utilisateur'
        ));
    }
}

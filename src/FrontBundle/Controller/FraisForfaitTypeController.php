<?php

namespace FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FrontBundle\Entity\FraisForfaitType;
use FrontBundle\Form\FraisForfaitTypeType;

/**
 * FraisForfaitType controller.
 *
 */
class FraisForfaitTypeController extends Controller
{
    /**
     * Lists all FraisForfaitType entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fraisForfaitTypes = $em->getRepository('FrontBundle:FraisForfaitType')->findAll();

        return $this->render('@Front/fraisforfaittype/index.html.twig', array(
            'fraisForfaitTypes' => $fraisForfaitTypes,
        ));
    }

    /**
     * Creates a new FraisForfaitType entity.
     *
     */
    public function newAction(Request $request)
    {
        $fraisForfaitType = new FraisForfaitType();
        $form = $this->createForm('FrontBundle\Form\FraisForfaitTypeType', $fraisForfaitType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fraisForfaitType);
            $em->flush();

            return $this->redirectToRoute('admin_fraisforfaittype_show', array('id' => $fraisForfaitType->getId()));
        }

        return $this->render('@Front/fraisforfaittype/new.html.twig', array(
            'fraisForfaitType' => $fraisForfaitType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a FraisForfaitType entity.
     *
     */
    public function showAction(FraisForfaitType $fraisForfaitType)
    {
        $deleteForm = $this->createDeleteForm($fraisForfaitType);

        return $this->render('@Front/fraisforfaittype/show.html.twig', array(
            'fraisForfaitType' => $fraisForfaitType,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing FraisForfaitType entity.
     *
     */
    public function editAction(Request $request, FraisForfaitType $fraisForfaitType)
    {
        $deleteForm = $this->createDeleteForm($fraisForfaitType);
        $editForm = $this->createForm('FrontBundle\Form\FraisForfaitTypeType', $fraisForfaitType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fraisForfaitType);
            $em->flush();

            return $this->redirectToRoute('admin_fraisforfaittype_edit', array('id' => $fraisForfaitType->getId()));
        }

        return $this->render('fraisforfaittype/edit.html.twig', array(
            'fraisForfaitType' => $fraisForfaitType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a FraisForfaitType entity.
     *
     */
    public function deleteAction(Request $request, FraisForfaitType $fraisForfaitType)
    {
        $form = $this->createDeleteForm($fraisForfaitType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fraisForfaitType);
            $em->flush();
        }

        return $this->redirectToRoute('admin_fraisforfaittype_index');
    }

    /**
     * Creates a form to delete a FraisForfaitType entity.
     *
     * @param FraisForfaitType $fraisForfaitType The FraisForfaitType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FraisForfaitType $fraisForfaitType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_fraisforfaittype_delete', array('id' => $fraisForfaitType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

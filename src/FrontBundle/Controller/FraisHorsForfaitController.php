<?php

namespace FrontBundle\Controller;

use FrontBundle\Form\FraisHorsForfaitType;
use FrontBundle\Entity\FraisHorsForfait;
use FrontBundle\Entity\FicheFrais;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;



class FraisHorsForfaitController extends Controller
{
    public function indexAction(Request $request, $id = null)
    {
        if($id == null){
            $fraisHorsForfait = new FraisHorsForfait();
        }else{
            $fraisHorsForfait = $this->getDoctrine()->getRepository('FrontBundle:FraisHorsForfait')->find($id);
        }

        $form = $this->createForm(FraisHorsForfaitType::class, $fraisHorsForfait);
        if($id == null){
            $form->add('Ajouter', SubmitType::class);
        }else{
            $form->add('Modifier', SubmitType::class);
        }


        $form->handleRequest($request);

        $fraisHorsForfaits = $this->getDoctrine()->getRepository('FrontBundle:FraisHorsForfait')->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fraisHorsForfait);
            $em->flush();

            return $this->redirectToRoute('test_fraisforfait_index');
        }

        return $this->render('@Front/fraisForfait/index.html.twig',
            array(
                'form' => $form->createView(),
                'fraisHorsForfaits' => $fraisHorsForfaits,
            ));
    }

    public function removefraisHorsForfaitAction(Request $request, $id)
    {

        $fraisHorsForfait = $this->getDoctrine()->getRepository('FrontBundle:FraisHorsForfait')->find($id);

        if ($fraisHorsForfait != null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fraisHorsForfait);
            $em->flush();
        }

        return $this->redirectToRoute('test_fraisforfait_index');
    }
}

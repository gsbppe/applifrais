<?php

namespace FrontBundle\Controller;

use FrontBundle\Entity\FicheFrais;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@Front/Default/index.html.twig');
    }
}
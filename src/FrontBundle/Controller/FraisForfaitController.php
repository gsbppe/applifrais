<?php

namespace FrontBundle\Controller;

use FrontBundle\Entity\FicheFrais;
use FrontBundle\Form\FraisForfaitType;
use FrontBundle\Entity\FraisForfait;
use FrontBundle\Form\FraisHorsForfaitType;
use FrontBundle\Entity\FraisHorsForfait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;


class FraisForfaitController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $annee = date("Y");
        $mois = date("m");
        $jour = date("d");
        $anneeFiche = $annee;
        if ($jour <= 10) {
            $moisFiche = $mois;
        } else {
            if ($mois == 12) {
                $moisFiche = 1;
                $anneeFiche = $annee + 1;
            } else {
                $moisFiche = $mois + 1;
            }
        }

        $laFiche = $this->getDoctrine()->getRepository('FrontBundle:FicheFrais')
            ->findOneby(array(
                'mois' => $moisFiche,
                'annee' => $anneeFiche,
                'utilisateur' => $user,
            ));


        if ($laFiche == null) {
            $laFiche = new FicheFrais();
            $laFiche->setMois($moisFiche);
            $laFiche->setAnnee($anneeFiche);
            $laFiche->setUtilisateur($user);


            $user->addFicheFrais($laFiche);

            $laFiche->setEtat($this->getDoctrine()->getRepository('FrontBundle:Etat')->findOneByOrdre(1));
        }

        $fraisForfait = new FraisForfait();
        $fraisForfait->setEtat($this->getDoctrine()->getRepository('FrontBundle:Etat')->findOneByOrdre(2));
        $formFF = $this->createForm(FraisForfaitType::class, $fraisForfait);
        $formFF->handleRequest($request);

        if ($formFF->isSubmitted() && $formFF->isValid()) {
            $laFiche->addFraisForfait($fraisForfait);
            $em->persist($user);
            $em->flush();
        }

        $fraisHorsForfait = new FraisHorsForfait();
        $fraisHorsForfait->setEtat($this->getDoctrine()->getRepository('FrontBundle:Etat')->findOneByOrdre(2));
        $formHF = $this->createForm(FraisHorsForfaitType::class, $fraisHorsForfait);
        $formHF->handleRequest($request);

        if ($formHF->isSubmitted() && $formHF->isValid()) {
            $laFiche->addFraisHorsForfait($fraisHorsForfait);
            $em->persist($user);
            $em->flush();
        }

        return $this->render('@Front/FraisForfait/index.html.twig', array(
            'laFiche' => $laFiche,
            'formFF' => $formFF->createView(),
            'formHF' => $formHF->createView(),
        ));



    }

    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $fraisForfait = $em->getRepository('FrontBundle:FraisForfait')->find($id);
        /**/
        $fraisHorsForfait = $em->getRepository('FrontBundle:FraisHorsForfait')->find($id);

        if ($fraisForfait) {
            $form = $this->createForm(FraisForfaitType::class, $fraisForfait);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($fraisForfait);
                $em->flush();
            }
        } elseif ($fraisHorsForfait) {
            $form = $this->createForm(FraisHorsForfaitType::class, $fraisForfait);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($fraisHorsForfait);
                $em->flush();
            }
        } else {
            throw $this->createNotFoundException(
                'Aucun frais pour id ' . $id
            );
        }
        return $this->render('@Front/FraisForfait/edit.html.twig');
        #return $this->redirectToRoute('test_fraisforfait_edit');

        /*
        return $this->render('@Front/FraisForfait/index.html.twig',
            array(
                'formFraisF' => $form->createView(),
                'fraisforfait' => $fraisForfait,

                'formFraisHF' => $formFraisHF->createView(),
                'fraisHorsforfait' => $fraisHorsForfait,
            )
        );
        */
    }

    public function removeAction($id)
    {

        $fraisForfait = $this->getDoctrine()->getRepository('FrontBundle:FraisForfait')->find($id);
        /**/
        $fraisHorsForfait = $this->getDoctrine()->getRepository('FrontBundle:FraisHorsForfait')->find($id);


        if ($fraisForfait != null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fraisForfait);
            $em->flush();
        }

        if ($fraisHorsForfait != null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fraisHorsForfait);
            $em->flush();
        }

        return $this->redirectToRoute('test_fraisforfait_index');
    }

    public function ajoutfraisAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
//        $date_jour = new \DateTime();
//        $annee = $date_jour->format("Y");
//        $mois = $date_jour->format("M");
//        $jour = $date_jour->format("d");
//        $moisFiche = $mois;
        $annee = date("Y");
        $mois = date("m");
        $jour = date("d");
        $anneeFiche = $annee;
        if ($jour <= 10) {
            $moisFiche = $mois;
        } else {
            if ($mois == 12) {
                $moisFiche = 1;
                $anneeFiche = $annee + 1;
            } else {
                $moisFiche = $mois + 1;
            }
        }

        $laFiche = $this->getDoctrine()->getRepository('FrontBundle:FicheFrais')
            ->findOneby(array(
                'mois' => $moisFiche,
                'annee' => $anneeFiche,
                'utilisateur' => $user,
            ));


        if ($laFiche == null) {
            $laFiche = new FicheFrais();
            $laFiche->setMois($moisFiche);
            $laFiche->setAnnee($anneeFiche);
            $laFiche->setUtilisateur($user);
            $laFiche->setEtat($this->getDoctrine()->getRepository('FrontBundle:Etat'));//->findBypriorite(10));
        }
        return $request;
    }
}

<?php

namespace FrontBundle\Controller;

use FrontBundle\Entity\FraisForfaitType;
use FrontBundle\Repository\FraisForfaitRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FrontBundle\Entity\Utilisateur;
use FrontBundle\Entity\FicheFrais;
use FrontBundle\Repository\FicheFraisRepository;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/*
 * PhoneApiController permet de gérer les envois de données de l'application mobile
 * */

class PhoneApiController extends Controller
{

    /**
     * @Route("/", name="login")
     * @param Request $request
     * @return JsonResponse
     */
    /*
     * connectAction gère la page de connexion
     * */
    public function connectAction(Request $request)
    {
        //récupération des logins de l'utilisateur dans la requête
        $username = $request->get('username');
        $password = $request->get('password');
        $success=false;
        $message="";

        //recherche de l'utilisateur dans la page
		$em = $this->get('doctrine')->getEntityManager();
		$user = $this->getDoctrine()
                    ->getRepository('FrontBundle:Utilisateur')
                    ->findOneByUsername($username);

        //récupération et vérification du password
        if($user != null) {
            //encodage du password
            $encoder_service = $this->get('security.encoder_factory');

            $encoder = $encoder_service->getEncoder($user);

            $success = $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());

            if ($success == true) {
                $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
                $context = $this->get('security.token_storage');
                $context->setToken($token);

                $message = $user->getId();
            } else {
                $message = "Connection refusée";
            }
        }else {
            $message = "Utilisateur inconnu";
        }
        //renvoi du message de connection (succès ou échec)
        $response = new JsonResponse();
        $response->setData(array(
            'success' => $success,
            'message' => $message,
        ));

        return $response;

    }

    /**
     * @Route("/", name="show")
     * @param Request $request
     * @return JsonResponse
     */
    /*
     * listFicheAction gère la page d'affichage des frais
     * */
    public function listFicheAction(Request $request)
    {
        //récupération de l'id du visiteur dans la requête
        $visiteur = $request->get('id');
        $success = false;
        $frais = 0;
        $date = getdate();
        $mois = $date['mon'];

        //récupération des fiches correspondant à cet id
        $em = $this->get('doctrine')->getEntityManager();
        $fichesFrais = $this->getDoctrine()
            ->getRepository('FrontBundle:FicheFrais')
            //->findByMois($mois);
            ->findByUtilisateur($visiteur);

        //récupération de la fiche du mois correspondant à cet id
        $em = $this->get('doctrine')->getEntityManager();
        $ficheFrais = $this->getDoctrine()
            ->getRepository('FrontBundle:FicheFrais')
            ->findOneBy(
                array('utilisateur' => $visiteur, 'mois' => $mois)
            );


/*
         * ça c'est de la couille mais je garde parce que ça peut servir //Matthieu
         *
        //idFiche désigne l'id d'une fiche
        $idFiche = [];
        $moisFiche = [];


        //à ce stade, $fichesFrais est un array de l'ensemble des fiches de l'utilisateur

        if($fichesFrais != null) {
            //si il y'a au moins une fiche, on récupère son id
            foreach ($fichesFrais as $ficheFrais){
                //remplissage tableau des id
                array_push($idFiche,$this->getDoctrine()
                    ->getRepository('FrontBundle:FraisForfait')
                    ->findById($ficheFrais->getId()));
                //remplissage tableau des mois
                array_push($moisFiche,$this->getDoctrine()
                    ->getRepository('FrontBundle:FraisForfait')
                    ->findByMois($ficheFrais->getMois()));
            }
            $success = true;
        }*/

        //ça c'est bon! // Matthieu aussi

        if($ficheFrais != null) {
            //ici on a la fiche de l'utilisateur pour le mois
            //récupération des frais forfait de la fiche
            $em = $this->get('doctrine')->getEntityManager();
            $lesFraisForfaits = $this->getDoctrine()
                ->getRepository('FrontBundle:FraisForfait')
                ->findByFicheFrais($ficheFrais->getId());
            //récupération des informations relatives aux frais et stockage dans un tableau
            $arrayFrais= array();
            foreach ($lesFraisForfaits as $unFrais){
                $leFrais = array(
                    'type' => $unFrais->getFraisForfaitType()->getLibelle(),
                    'prix_unitaire' => $unFrais->getFraisForfaitType()->getPrixUnitaire(),
                    'quantite' => $unFrais->getQuantite(),
                    /*TODO :
                    - corriger la date renvoyé (actuellement date par défaut 2012/01/01)
                    - récupérer le libellé de l'état
                     * 'date' => $unFrais->getDateFrais(),
                    'etat' => $unFrais->getEtat()*/
                );
                array_push($arrayFrais, $leFrais);
            }
            $success = true;

        }else{
            $arrayFrais = "Aucun Frais enregistré pour la fiche";
        }
        //renvoi des données relatives àla fiche frais : id, mois, tableau des frais
        $response = new JsonResponse();
        $response->setData(
            array(
            'success' => $success,
            'id_fiche' => $ficheFrais->getId(),
            'mois_fiche' => $ficheFrais->getMois(),
            'frais_forfait' => $arrayFrais,
            )
        );
        return $response;
    }

    /*
     * detailListFicheAction : ça reste à faire mais en vrai ça risque d'être useless
     * puisqu'on affiche les détails des frais en même temps que la fiche lol
     * */
    public function detailListFicheAction(Request $request)
    {
        $idFiche = $request->get('idFiche');
        $success=false;
        $message="";

        $em = $this->get('doctrine')->getEntityManager();
        $fiche = $this->getDoctrine()
            ->getRepository('FrontBundle:FicheFrais')
            ->findOneBy(array('id' => $idFiche));

        if($fiche != null) {
            $message = $fiche->getFraisForfait();

        }else {
            $message = "Aucun frais enregistré";
        }
        $response = new JsonResponse();
        $response->setData(array(
            'success' => $success,
            'message' => $message,
        ));

        return $response;
    }

}
